/*
SPDX-FileCopyrightText: 2021 Hamed Masafi <hamed.masfi@gmail.com>

SPDX-License-Identifier: GPL-3.0-or-later
*/

#pragma once

#include "abstractcommand.h"
#include "libgitklient_export.h"
namespace Git
{

class LIBGITKLIENT_EXPORT CommandCommit : public AbstractCommand
{
public:
    CommandCommit();
    QStringList generateArgs() const override;

    const QString &message() const;
    void setMessage(const QString &newMessage);

    bool amend() const;
    void setAmend(bool newAmend);

    OptionalBool includeStatus() const;
    void setIncludeStatus(OptionalBool newIncludeStatus);

private:
    QString mMessage;
    bool mAmend{false};
    OptionalBool mIncludeStatus{false};
};

} // namespace Git
